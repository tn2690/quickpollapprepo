package com.tn2690.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tn2690.domain.Option;

@Repository
public interface OptionRepository extends JpaRepository<Option, Long> {

}
